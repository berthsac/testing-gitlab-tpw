<?php

  require 'conexion.php';

  $message = '';

  if (!empty($_POST['email']) && !empty($_POST['password'])) {
    $sql = "INSERT INTO user (email, password) VALUES (:email, :password)";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':email', $_POST['email']);
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $stmt->bindParam(':password', $password);

    if ($stmt->execute() && $password = "confirm_password") {
      $message = 'Bienvenido';
      header("location:login.php");
    } else {
      $message = 'Problema al crear su cuenta';
    }
  }
?>
<!DOCTYPE html>
<html lang="en-GB">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="Ejemplo de HTML5">
        <meta name="keywordas" content="HTML5,CSS3,JavaScript">
        <title>Viaja por el Mundo</title>
        <link rel="stylesheet" href="./CSS/estilos.css">
       
    </head>
    
    <body style="background-color: lightgrey ;" >
             
   
        <div id="header">

        <nav class="networks">
            <a href=""><img src="./Imagen/youtube.png" width="50px" height="50px" class="right" /></a>  
            <a href=""><img src="./Imagen/twitter.png" width="50px" height="50px"  class="right"/></a> 
            <a href="https://www.instagram.com/bertha_cs/"><img src="./Imagen/instagram.png" width="50px" height="50px"  class="right"/></a>    
        </nav>
            
        <div class="container logo-nav-container" >
            <img src="./Imagen/dear.png" width="100px" height="120px"  class="right"/>
            <h1>Nómada</h1>    
        </div>  

        <center>
            <nav class="menu">
            <ul>
                    <li><a href="index.html" style="text-decoration: none;"><h3>INICIO</h3></a></li> 
                    <li><a href="about.html" style="text-decoration: none;"><h3>ACERCA</h3></a></li> 
                    <li><a href="briefcase.html" style="text-decoration: none;"><h3>PORTAFOLIO</h3></a></li> 
                    <li><a href="contact.html"style="text-decoration: none;"><h3>CONTACTO</h3></a></li> 
            </ul>
            </nav>
        </center>

</div>
    <?php if(!empty($message)): ?>
      <p> <?= $message ?></p>
    <?php endif; ?>

    <center><h1>Formulario de registro</h1></center>
    <center>
        <div id="formulario">
        <form action="signup.php" method="POST">
        <input name="email" type="text" placeholder="Enter your email"></br>
        <input name="password" type="password" placeholder="Enter your Password"></br>
        <input name="confirm_password" type="password" placeholder="Confirm Password"></br>
        <input type="submit" value="Registrar">
        <span>o <a href="login.php">Iniciar sesion</a></span>
        </form>
        </div>
    <center>

    <center><nav class="pie">
            
            <ul>
                <li><div id="page_content">
                    <a href="about.html" style="text-decoration: none;"><h3>Acerca</h3></a>
                    <a>¿Quien soy?</a></br>
                    <a>Fotos</a></br>
                    <a>Videos</a></br>
                </div></li>
            
                <li><div id="portfolio_page">
                    <a href="briefcase.html" style="text-decoration: none;"><h3>Portafolio</h3></a>
                    <a>Logos</a></br>
                    <a>Paquetes</a></br>
                    <a>Promocional </a></br>
                    <a>Fotografias</a></br>
                </div></li>
            
                <li><div id="pag_contacto">
                <h3>Website designed by Bertha Chavez Salazar</h3>
                <img src="./Imagen/berth.jpeg" width="100px" height="130px"/></br>
                <a href="contact.html"style="text-decoration: none;">Contacto</a></br> 
            </div></li>
            </ul>
       
        </nav></center>
  </body>
</html>
