<?php

  require 'conexion.php';

  $message = '';

  if (!empty($_POST['email']) && !empty($_POST['nombre'])) {
    $sql = "INSERT INTO contacto (nombre, apellidos, email, telefono,nacionalidad,asunto,mensaje) VALUES (:nombre, :apellidos, :email, :telefono,:nacionalidad, :asunto, :mensaje)";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':nombre', $_POST['nombre']);
    $stmt->bindParam(':apellidos', $_POST['apellidos']);
    $stmt->bindParam(':email', $_POST['email']);
    $stmt->bindParam(':telefono', $_POST['telefono']);
    $stmt->bindParam(':nacionalidad', $_POST['nacionalidad']);
    $stmt->bindParam(':asunto', $_POST['asunto']);
    $stmt->bindParam(':mensaje', $_POST['mensaje']);

    if ($stmt->execute()) {
      $message = 'Su mensaje ha sido enviado, en breve nos pondremos en contacto con usted. GRACIAS!';
    } else {
      $message = 'Problema al enviar su mensaje';
    }
  }
?>
<!DOCTYPE html>
<html lang="en-GB">
    <head>
        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <meta name="description" content="Ejemplo de HTML5">
        <meta name="keywordas" content="HTML5,CSS3,JavaScript">
        <title>Contact</title>
        <link rel="stylesheet" href="./CSS/estilos.css">
    </head>
    
    <body style="background-color: lightgrey ;" >
               
    <div id="header">
        
        <nav class="networks">
            <a href=""><img src="./Imagen/youtube.png" width="50px" height="50px" class="right" /></a>  
            <a href=""><img src="./Imagen/twitter.png" width="50px" height="50px"  class="right"/></a> 
            <a href="https://www.instagram.com/bertha_cs/"><img src="./Imagen/instagram.png" width="50px" height="50px"  class="right"/></a>    
        </nav>
        
        <div class="container logo-nav-container" >
            <img src="./Imagen/dear.png" width="100px" height="120px"  class="right"/>
            <h1>Nómada</h1>  
        </div>  
      
        <center>
            <nav class="menu">
            <ul>
                    <li><a href="index.html" style="text-decoration: none;"><h3>INICIO</h3></a></li> 
                    <li><a href="about.html" style="text-decoration: none;"><h3>ACERCA</h3></a></li> 
                    <li><a href="briefcase.html" style="text-decoration: none;"><h3>PORTAFOLIO</h3></a></li> 
                    <li><a href="contact.php"style="text-decoration: none;"><h3>CONTACTO</h3></a></li> 
            </ul>
            </nav>
        </center>
        
    </div>
    
    
    <?php if(!empty($message)): ?>
      <p> <?= $message ?></p>
    <?php endif; ?>
    
    <center><h1 class="letter">CONTACTANOS PARA TU PROXIMO VIAJE</h1></center>
   
    <center><div id="formulario">
    <form action="contact.php" method="POST" >
        <label><input name="nombre" id="nombre" placeholder="Nombre*" type="text"></label></br>
        <label><input name="apellidos" id="apellidos" placeholder="Apellidos*" type="text"></label></br>
        <label><input name="email" id="email" placeholder="Correo electronico*" type="text"></label></br>
        <label><input name="telefono" id="telefono" placeholder="Teléfono" type="text"></label></br>
        <label><input name="nacionalidad" id="nacionalidad" placeholder="Nacionalidad" type="text"></label></br>
        <label><input name="asunto" id="asunto" placeholder="Asunto*" type="text"></label></br>
        <p></p>
        <textarea id="mensaje" name="mensaje" placeholder="Mensaje*" cols="54" rows="5"></textarea></br>
        <input type="submit">
    </form>
    </div>
    
    </center>

    <h2>Tiempo para finalizar este dia: </h2>
    <FORM NAME="Reloj">
      <INPUT TYPE="text" SIZE="7" NAME="tiempo" VALUE="mm:hh:ss" TITLE="Tiempo restante para finalizar el dia">
      <SCRIPT LANGUAGE="JavaScript">
 
        var tiempoAtras;
        updateReloj();
 
        function updateReloj()
        {
          var tiempo = new Date();
          var hora = 23-tiempo.getHours();
          var minutos = 59-tiempo.getMinutes();
          var segundos = 59-tiempo.getSeconds();
 
          tiempoAtras = (hora < 10) ? hora :hora;
          tiempoAtras += ((minutos < 10) ? ":0":":") + minutos;
          tiempoAtras += ((segundos < 10) ? ":0":":") + segundos;
 
          document.Reloj.tiempo.value = tiempoAtras;
          setTimeout("updateReloj()",1000);
        }
      </SCRIPT>   
    </FORM> 
    <center><a href="javascript:(function(){alert('No te vayas sin contactarnos')})()">Salir</a></center>
    
        <center><nav class="contact">
  
            <ul>
                <li><div id="phone">
                <img src="./Imagen/smartphone.png" width="50px" height="50px"/>
                <p>TELEFONO: 957046158</p>
                </div></li>
            <li><div id="mailbox">
                <img src="./Imagen/mailbox.png" width="50px" height="50px"/>
                    <p>CORREO: berthsac@gmail.com</p>
                      
            </div></li>
            </ul>
        </nav></center>

        <a href="javascript:void(window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location)));">Compartir en facebook</a>
        
        <center><nav class="pie">
            
            <ul>
                <li><div id="page_content">
                    <a href="about.html" style="text-decoration: none;"><h3>Acerca</h3></a>
                    <a>¿Quien soy?</a></br>
                    <a>Fotos</a></br>
                    <a>Videos</a></br>
                </div></li>
            
                <li><div id="portfolio_page">
                    <a href="briefcase.html" style="text-decoration: none;"><h3>Portafolio</h3></a>
                    <a>Logos</a></br>
                    <a>Paquetes</a></br>
                    <a>Promocional </a></br>
                    <a>Fotografias</a></br>
                </div></li>
            
                <li><div id="pag_contacto">
                <h3>Website designed by Bertha Chavez Salazar</h3>
                <img src="./Imagen/berth.jpeg" width="100px" height="130px"/></br>
                <a href="contact.html"style="text-decoration: none;">Contacto</a></br> 
            </div></li>
            </ul>
       
        </nav></center>
        
    </body>
</html>

    

  