<?php

  session_start();

  if (isset($_SESSION['user_id'])) {
    header('Location: /MiPaginaWeb/login.php');
  }
  require 'conexion.php';

  if (!empty($_POST['email']) && !empty($_POST['password'])) {
    $records = $conn->prepare('SELECT id, email, password FROM user WHERE email = :email');
    $records->bindParam(':email', $_POST['email']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);

    $message = '';

    if (count($results) > 0 || password_verify($_POST['password'], $results['password'])) {
      $_SESSION['user_id'] = $results['id'];
      header("location:clientes.html");
      } else {
      $message = 'Lo siento, estas credenciales no coinciden';
    }
  }

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Bienvenido a Nómade</title>
    <meta name="keywordas" content="HTML5,CSS3,JavaScript">
    <title>Viaja por el Mundo</title>
    <link rel="stylesheet" href="./CSS/estilos.css">
  </head>
  <body style="background-color: lightgrey ;">
  <div id="header">

        <nav class="networks">
            <a href=""><img src="./Imagen/youtube.png" width="50px" height="50px" class="right" /></a>  
            <a href=""><img src="./Imagen/twitter.png" width="50px" height="50px"  class="right"/></a> 
            <a href="https://www.instagram.com/bertha_cs/"><img src="./Imagen/instagram.png" width="50px" height="50px"  class="right"/></a>    
        </nav>
            
        <div class="container logo-nav-container" >
            <img src="./Imagen/dear.png" width="100px" height="120px"  class="right"/>
            <h1>Nómada</h1>    
        </div>  
        </br>
    
    <center>
        <nav class="menu">
        <ul>
                <li><a href="index.html" style="text-decoration: none;"><h3>INICIO</h3></a></li> 
                <li><a href="about.html" style="text-decoration: none;"><h3>ACERCA</h3></a></li> 
                <li><a href="briefcase.html" style="text-decoration: none;"><h3>PORTAFOLIO</h3></a></li> 
                <li><a href="contact.php"style="text-decoration: none;"><h3>CONTACTO</h3></a></li> 
        </ul>
        </nav>
    </center>
    <p></p>
    <center><h1>Iniciar sesion</h1></center>
    </div>
    <center>
        <div action="login.php" id="formulario">
        <?php if(!empty($message)): ?>
          <p> <?= $message ?></p>
        <?php endif; ?>
        <form action="login.php" method="POST">
         <input id="email" name="email" type="text" placeholder="Ingrese su correo" >
         <input id="password" name="password" type="password" placeholder="Ingrese su contraseña">
          <input type="submit" value="Iniciar">
          
        </form>
        <span>o <a style="text-decoration: none;" href="signup.php">Registrarse</a></span>
    <center>

    </div>

    <center><nav class="pie">
            
            <ul>
                <li><div id="page_content">
                    <a href="about.html" style="text-decoration: none;"><h3>Acerca</h3></a>
                    <a>¿Quien soy?</a></br>
                    <a>Fotos</a></br>
                    <a>Videos</a></br>
                </div></li>
            
                <li><div id="portfolio_page">
                    <a href="briefcase.html" style="text-decoration: none;"><h3>Portafolio</h3></a>
                    <a>Logos</a></br>
                    <a>Paquetes</a></br>
                    <a>Promocional </a></br>
                    <a>Fotografias</a></br>
                </div></li>
            
                <li><div id="pag_contacto">
                <h3>Website designed by Bertha Chavez Salazar</h3>
                <img src="./Imagen/berth.jpeg" width="100px" height="130px"/></br>
                <a href="contact.html"style="text-decoration: none;">Contacto</a></br> 
            </div></li>
            </ul>
       
        </nav></center>
  </body>
</html>
